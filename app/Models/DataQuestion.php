<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataQuestion extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'instructions',
        'questionaire_id',
        'key',
        'type'
    ];

    public function questionaire() {
      return $this->belongsTo(DataContainer::class);
    }

    public function replies() {
      return $this->hasMany(DataQuestionReply::class);
    }
}
