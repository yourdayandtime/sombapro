<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataQuestionReply extends Model
{
    use HasFactory;
    protected $fillable = [
      'answer',
      'data_question_id'
    ];

    public function DataQuestion() {
      return $this->belongsTo(DataQuestion::class);
    }
}
