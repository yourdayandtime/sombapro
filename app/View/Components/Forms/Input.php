<?php

namespace App\View\Components\Forms;

use Illuminate\View\Component;

class Input extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
     public $label;
     public $type;
     public $name;
     public $required;
     public $value;
     public $options;
     Public $placeholder ='';
    public function __construct($type='text', $required = false, $placeholder = '', $label ='', $name, $value ='', $options = [])
    {
        $this->type = $type;
        $this->required = $required;
        $this->name = $name;
        $this->label = $label;
        $this->value = $value;
        $this->options = $options;
        $this->placeholder = $placeholder;
    }

    public function isSelected($option)
    {
        return $option === $this->selected;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.forms.input');
    }
}
