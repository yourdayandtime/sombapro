<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Business;
use App\Models\BusinessCategory;
use App\Models\ProductCategory;

class CmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function __construct()
   {
       $this->middleware('auth');
   }

    public function index()
    {
        //
    }

    public function users()
    {
        $users = User::orderBy('name','ASC')->get();
        $businesses = Business::orderBy('name','ASC')->get();
        $businessCategories = BusinessCategory::orderBy('name','ASC')->get();
        $productCategories = ProductCategory::orderBy('name','ASC')->get();
        return view('user.list')->with(
          [
            'users' => $users,
            'businesses' => $businesses,
            'business_categories' => $businessCategories,
            'product_categories' => $productCategories,
          ]
        );
    }
}
