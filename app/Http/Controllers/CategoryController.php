<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BusinessCategory;
use App\Models\ProductCategory;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use DataTables;

class CategoryController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function __construct()
  {
    $this->middleware('auth');
  }
  public function index(Request $request)
  {
    $productCategories = ProductCategory::orderBy('name','ASC')->get();
    $businessCategories = BusinessCategory::orderBy('name','ASC')->get();
    if ($request->ajax()) {
      $data = BusinessCategory::latest()->get();
      return Datatables::of($data)
      ->addIndexColumn()
      ->addColumn('action', function($row){
        $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editCategory"><i class="fa fa-edit"></i></a>';
        $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteCategory"><i class="fa fa-trash"></i></a>';
        return $btn;
      })
      ->rawColumns(['action'])
      ->make(true);
    }
    return view('category.table-list');
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    return redirect()->route('category.index');
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    $category = BusinessCategory::updateOrCreate($request->all());
    return response()->json(['success'=>'Customer saved successfully!']);

    // $rules = [
    //   'name' => ['required','max:255'],
    //   'description' => ['max:500'],
    //   'category_model' => [
    //     'required',
    //     Rule::in(['Business', 'Product'])
    //   ]
    // ];
    //
    // $request->validate($rules);
    //
    // $data = [
    //   'name' => $request->name,
    //   'description' => $request->description
    // ];
    //
    // if($request->category_model === 'Business') {
    //   $created = BusinessCategory::create($data);
    // } else {
    //   $created = ProductCategory::create($data);
    // }
    //
    // if($created) {
    //   //session()->flash('success','Created successfully!');
    //   return redirect()->route('category.index')->withSuccess('Category created successfully!');
    // } else{
    //    return back()->withInput()->withErrors('Something went wrong. Please try again later.');
    // }
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $category = BusinessCategory::find($id);
    return response()->json($category);
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $category = BusinessCategory::find($id);
    $category->name = $request->name;
    $category->description = $request->description;
    $category->save();
    return response()->json(['success'=>'Customer saved successfully!']);
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    BusinessCategory::destroy($id);
    return response()->json(['success'=>'Category deleted!']);
  }
}
