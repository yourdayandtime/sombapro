<?php

namespace App\Http\Controllers;

use App\Models\DataContainer;
use Illuminate\Http\Request;

class DataContainerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DataContainer  $dataContainer
     * @return \Illuminate\Http\Response
     */
    public function show(DataContainer $dataContainer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DataContainer  $dataContainer
     * @return \Illuminate\Http\Response
     */
    public function edit(DataContainer $dataContainer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DataContainer  $dataContainer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DataContainer $dataContainer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DataContainer  $dataContainer
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataContainer $dataContainer)
    {
        //
    }
}
