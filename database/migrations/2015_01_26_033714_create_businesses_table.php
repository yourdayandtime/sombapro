<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {
          $table->id();
          $table->string('name');
          $table->string('description', 1000);
          $table->string('country')->nullable();
          $table->string('province')->nullable();
          $table->string('city')->nullable();
          $table->string('address')->nullable();
          $table->string('app_name')->nullable();
          $table->string('status');
          $table->string('logo_path')->nullable();
          $table->timestamp('stopped_since')->nullable();
          $table->foreignId('user_id');
          $table->timestamps();
          $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('businesses');
    }
}
