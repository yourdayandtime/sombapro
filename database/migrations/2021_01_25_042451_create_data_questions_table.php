<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_questions', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('instructions', 500)->nullable();
            $table->string('type', 20)->nullable();//input type
            $table->string('key')->nullable();//for readable url
            $table->foreignId('questionaire_id');
            $table->timestamps();
            $table->foreign('questionaire_id')->references('id')->on('questionaires');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_questions');
    }
}
