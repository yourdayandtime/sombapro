<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataQuestionRepliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_question_replies', function (Blueprint $table) {
            $table->id();
            $table->json('answer')->nullable();
            $table->foreignId('data_question_id');
            $table->timestamps();
            $table->foreign('data_question_id')->references('id')->on('data_questions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_question_replies');
    }
}
