<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->create();
        \App\Models\Questionaire::factory(10)->create();
        \App\Models\Business::factory(10)->create();
        \App\Models\BusinessCategory::factory(10)->create();
        \App\Models\ProductCategory::factory(10)->create();
    }
}
