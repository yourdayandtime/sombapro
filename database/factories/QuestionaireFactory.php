<?php

namespace Database\Factories;

use App\Models\Questionaire;
use Illuminate\Database\Eloquent\Factories\Factory;

class QuestionaireFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Questionaire::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
      $title = $this->faker->sentence(3);
      $key = implode('-', explode(" ", $title));
      return [
        'title' => $title,
        'description' => $this->faker->paragraph(1),
        'key' => $key,
        'type' => $this->faker->randomElement(['register','profile']),
        'app_name' => $this->faker->randomElement(['morepromo','ydt']),
        'status' => $this->faker->randomElement(['active','inactive'])
      ];
    }
}
