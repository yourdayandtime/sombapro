<?php

namespace Database\Factories;

use App\Models\Business;
use Illuminate\Database\Eloquent\Factories\Factory;

class BusinessFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Business::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
     public function definition()
     {
         return [
           'name' => $this->faker->company(),
           'description' => $this->faker->paragraph(1),
           'country' => $this->faker->country(),
           'city' => $this->faker->city(),
           'address' => $this->faker->address(),
           'user_id'=> $this->faker->numberBetween(1,10),
           'app_name' => $this->faker->randomElement(['sombapromo','sombapromo_admin']),
           'status' => $this->faker->randomElement(['active','inactive'])
         ];
     }
}
