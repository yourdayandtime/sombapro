@extends('layouts.datatable')
@section('content')
  <div class="container">
    <h1>Categories </h1>
    <a class="btn btn-success mb-4 mt-2" href="javascript:void(0)" id="createNewCategory"> <i class="fa fa-plus"></i>  Create New Category</a>
    <table class="table table-bordered data-table">
      <thead>
        <tr>
          <!-- <th>No</th> -->
          <th>Name</th>
          <th>Description</th>
          <th width="100px">Action</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>

  <div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="modelHeading"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="categoryForm" name="categoryForm" class="form-horizontal">
            <input type="hidden" name="id" id="category_id">
            <div class="form-group">
              <label for="name" class="col-sm-2 control-label">Name</label>
              <div class="col-sm-12">
                <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" value="" maxlength="50" required="">
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label">Description</label>
              <div class="col-sm-12">
                <textarea id="description" name="description" placeholder="Description" class="form-control"></textarea>
              </div>
            </div>

            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-primary" id="saveBtn" value="create"><i class="fa fa-save"></i> Save
              </button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('javascript')
<script type="text/javascript">
  $(function () {
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "",
        columns: [
            //{data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'name', name: 'name'},
            {data: 'description', name: 'description'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    $('#createNewCategory').click(function () {
        $('#saveBtn').val("create-categoryr");
        $('#category_id').val('');
        $('#categoryForm').trigger("reset");
        $('#modelHeading').html("Create New Category");
        $('#ajaxModel').modal('show');
    });

    $('body').on('click', '.editCategory', function () {
      var category_id = $(this).data('id');
      $.get('/category/' + category_id + '/edit', function (data) {
          $('#modelHeading').html("Edit Category");
          $('#saveBtn').val("edit-category");
          $('#ajaxModel').modal('show');
          $('#category_id').val(data.id);
          $('#name').val(data.name);
          $('#description').val(data.description);
      })
   });

    $('#saveBtn').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
        let url = "/category";
        let action = "POST";
        if($(this).val() === 'edit-category') {
          let id = $('#category_id').val();
          url = "/category/" + id;
          action = "PUT";
        }
        $.ajax({
          data: $('#categoryForm').serialize(),
          url: url,
          type: action,
          dataType: 'json',
          success: function (data) {

              $('#categoryForm').trigger("reset");
              $('#ajaxModel').modal('hide');
              $('#saveBtn').html('Save Changes');
              table.draw();

          },
          error: function (data) {
              console.log('Error:', data);
              $('#saveBtn').html('Save Changes');
          }
      });
    });

    $('body').on('click', '.deleteCategory', function () {

        var category_id = $(this).data('id');
        if(confirm("Are You sure want to delete !")) {
          $.ajax({
              type: "DELETE",
              url: "/category/"+category_id,
              success: function (data) {
                  table.draw();
              },
              error: function (data) {
                  console.log('Error:', data);
              }
          });
        }
    });

  });
</script>
@endsection
