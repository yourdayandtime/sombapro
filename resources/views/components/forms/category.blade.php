<form method="post" action="{{ Route('category.store') }}">
  @csrf
  <h3>Cateory Details</h3>
  <div class="form-group">
    <label for="name">{{ __('Name') }}</label>
    <input type="text" name="name" required class="form-control @error('name') is-invalid @enderror" id="category-name">
    @error('name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
    <label for="description">{{ __('Description') }}</label>
    <textarea name="description" class="form-control @error('description') is-invalid @enderror" rows="2"></textarea>
    @error('description')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
    <label for="category-model">{{ __('Use category on') }}</label>
    <select class="form-control @error('category_model') is-invalid @enderror" required name="category_model">
      <option>Select...</option>
      <option value="Product">Product</option>
      <option value="Business">Business</option>
    </select>
    @error('category_model')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
    <button type="submit" class="btn btn-primary mt-1 btn-lg" name="button">Save</button>
  </div>
</form>
