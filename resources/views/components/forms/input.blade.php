@if(isset($label) && !empty($label))
<label for="{{ $name }}">{{ $label }}</label>
@endif
@switch($type)
    @case('select')
      <select name="{{ $name }}" class="form-control" {{ $required ? 'required' : ''}} id="{{ $name .'-id'}}">
        <option value>{{ $placeholder ? $placeholder : 'Select'}}</option>
        @foreach($options as $key => $option)
        <option value="{{ $key }}"> {{ $option }}</option>
        @endforeach
      </select>
        @break
    @case('radio')
        @foreach($options as $key => $option)
            <label for="{{ $name }}">{{ $option }}
              <input type="{{ $type }}" value="{{ $key }}" {{ $required ? 'required' : ''}} name="{{ $name }}" class="form-control"/>
            </label>
        @endforeach
        @break
    @case('checkbox')
        @foreach($options as $key => $option)
            <label for="{{ $name }}">{{ $option }}
              <input type="{{ $type }}" value="{{ $key }}" {{ $required ? 'required' : ''}} name="{{ $name }}" class="form-control"/>
            </label>
        @endforeach
        @break
    @case('textarea')
        <label for="{{ $name }}">{{ $option }}</label>
        <textarea  placeholder="{{ $placeholder ? $placeholder : ''}}" name="{{ $name }}" rows="4" id="{{ $name .'-id' }}" {{ $required ? 'required' : ''}}cols="80"></textarea>
        @break
    @default
        <input type="{{ $type }}" name="{{ $name }}"{{ $required ? 'required' : ''}} class="form-control" id="{{ $name .'-id'}}" placeholder="{{ $placeholder ? $placeholder : ''}}">
@endswitch
