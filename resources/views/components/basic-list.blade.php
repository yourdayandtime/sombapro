<div>
    <ol class="list-group list-group-flush">
      @foreach ($options as $key => $value)
        <li class="list-group-item">{{ $value }}</li>
      @endforeach
    </ol>
</div>
