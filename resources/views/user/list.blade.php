      @extends('layouts.app')
      @section('content')
      <div class="container-fluid">
          <div class="row justify-content-center">
              <div class="col-md-3">
                  <div class="card-body">
                    <h3>Users</h3>
                    @if(empty($users))
                    <div class="alert alert-warning">
                      <p>No user found.</p>
                    </div>
                    @else
                    <form>
                      <div class="form-group">
                        <input type="user" class="form-control" id="user" placeholder="Search...">
                      </div>
                    </form>
                    <ol class="list-group list-group-flush">
                      @foreach ($users as $user)
                        <li class="list-group-item">{{ $user->name }}</li>
                      @endforeach
                    </ol>
                </div>
                @endif
              </div>
              <div class="col-md-3">
                  <div class="card-body">
                    <h3>Businesses</h3>
                    @if(empty($businesses))
                    <div class="alert alert-warning">
                      <p>No business found.</p>
                    </div>
                    @else
                    <form>
                      <div class="form-group">
                        <input type="user" class="form-control" id="user" placeholder="Search...">
                      </div>
                    </form>
                    <ol class="list-group list-group-flush">
                      @foreach ($businesses as $business)
                        <li class="list-group-item">{{ $business->name }}<br/>{{ $business->address }}</li>
                      @endforeach
                    </ol>
                </div>
                @endif
              </div>
              <div class="col-md-3">
                  <div class="card-body">
                    <h3>Business Categories</h3>
                    @if(empty($business_categories))
                    <div class="alert alert-warning">
                      <p>No business category found.</p>
                    </div>
                    @else
                    <form>
                      <div class="form-group">
                        <input type="user" class="form-control" id="user" placeholder="Search...">
                      </div>
                    </form>
                    <ol class="list-group list-group-flush">
                      @foreach ($business_categories as $b_cat)
                        <li class="list-group-item">{{ $b_cat->name }}</li>
                      @endforeach
                    </ol>
                </div>
                @endif
              </div>
              <div class="col-md-3">
                @if(empty($product_categories))
                <div class="alert alert-warning">
                  <p>No product category found.</p>
                </div>
                @else
                  <div class="card-body">
                    <h3>Product Categories</h3>
                    <form>
                      <div class="form-group">
                        <input type="user" class="form-control" id="user" placeholder="Search...">
                      </div>
                    </form>
                    <ol class="list-group list-group-flush">
                      @foreach ($product_categories as $pro_cat)
                        <li class="list-group-item">{{ $pro_cat->name }}</li>
                      @endforeach
                    </ol>
                </div>
                @endif
              </div>
          </div>
      </div>
      @endsection
