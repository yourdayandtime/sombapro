<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\IndexController::class, 'index'])->name('index');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');
Route::get('/cms/users', [App\Http\Controllers\CmsController::class, 'users'])->name('cms.users');
Route::resources(
  [
    'user' => App\Http\Controllers\UserController::class,
    'product' => App\Http\Controllers\ProductController::class,
    'category' => App\Http\Controllers\CategoryController::class,
    'promotion' => App\Http\Controllers\PromotionController::class,
    'business' => App\Http\Controllers\BusinessController::class,
    'questionaire' => App\Http\Controllers\QuestionaireController::class,
    'question' => App\Http\Controllers\DataQuestionController::class,
    'reply' => App\Http\Controllers\DataQuestionReplyController::class
  ]
);
